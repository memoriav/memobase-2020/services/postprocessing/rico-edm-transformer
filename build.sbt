import Dependencies.*

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := {
  tag: String =>
    if (tag matches "[0-9]+\\..*") {
      Some(tag)
    } else {
      None
    }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "rico2edm",
    assembly / assemblyJarName := "app.jar",
    assembly / mainClass := Some("ch.memobase.rico2edm.Main"),
    assembly / test := {},
    assembly / assemblyExcludedJars := {
      val cp = (assembly / fullClasspath).value
      cp filter { f =>
        f.data.getName == "jcl-over-slf4j-1.7.30.jar" ||
        f.data.getName == "commons-codec-1.13.jar" ||
        f.data.getName == "commons-logging-1.2.jar"
      }
    },
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"                           => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case "log4j2.xml"                                 => MergeStrategy.first
      case PathList("META-INF", "versions", "9", "module-info.class") =>
        MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticLoggerBinder.class") =>
        MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticMDCBinder.class") =>
        MergeStrategy.first
      case PathList("org", "slf4j", "impl", "StaticMarkerBinder.class") =>
        MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    git.useGitDescribe := true,
    Test / fork := true,
    Test / envVars := Map(
      "TOPIC_IN" -> "",
      "TOPIC_OUT" -> "",
      "TOPIC_REPORT" -> "",
      "REPORTING_STEP_NAME" -> "",
      "RECORD_SET_INDEX" -> "",
      "INSTITUTION_INDEX" -> "",
      "ELASTIC_HOST" -> "",
      "ELASTIC_PORT" -> "",
      "EXPORT_DEFINITION_RULES_PATH" -> "",
      "INSTITUTION_COORDINATES_PATH" -> "",
      "ISOCODE_MAPPING_PATH" -> ""
    ),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      dataModelExtraction,
      esJavaClient,
      http4sDsl,
      http4sServer,
      kafkaStreams.cross(CrossVersion.for3Use2_13),
      loggingCommons,
      log4jApi,
      log4jCore,
      log4jScala,
      log4jSlf4j,
      rdf4jmodel,
      rioapi,
      rioapixml,
      upickle,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      scalatic % Test
    )
  )
