/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.Utils.{loadFile, parseRecordFile}
import ch.memobase.rico2edm.edm.subjects.{ProvidedCHO, WebResource}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class GetErrorSpec extends AnyFunSuite with Matchers {

  private lazy val noDigitalObject = parseRecordFile(
    "src/test/resources/raw.get.error.json"
  )

  test("hunt get raises None error") {
    assert(noDigitalObject.digitalObject.isEmpty)

    /*
    val record = Extractors.record(graph).get

    val cho = new ProvidedCHO(Extractors.recordId(record).get)
    cho.addTitel(Extractors.title(record))
    cho.addDescription(Extractors.dctAbstract(record))
    cho.addDescription(Extractors.scopeAndContent(record))
    cho.addDescription(Extractors.descriptiveNote(record))
    cho.addCreationDate(Extractors.creationDate(graph)(record.obj))


    val webExtraction = Extractors.dobjectId(digitalObject).map(id => List[WebResource](new WebResource(id))).getOrElse(List())

    webExtraction.foreach(webResource =>
      cho.getModel.addAll(webResource.getModel)
    )

     */

  }

}
