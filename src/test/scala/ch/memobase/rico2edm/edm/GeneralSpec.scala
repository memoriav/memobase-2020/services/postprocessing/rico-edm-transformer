/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.memobase.rico2edm.edm

import ch.memobase.Utils.loadFile
import ch.memobase.rico2edm.KafkaTopologyUtils
import ch.memobase.rico2edm.utils.Helper
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class GeneralSpec extends AnyFunSuite with Matchers {

  private lazy val apf001 = loadFile("src/test/resources/apf-001-1280.json")
  private lazy val noapf = loadFile("src/test/resources/rico.duration.json")
  private lazy val definedInstitution = loadFile("src/test/resources/rawIndent.no.contributor.json")


  ignore("is part of europeana export") {
    Helper.initOaiExportRules("src/test/resources/exportrules/rules.json")

    assert(!KafkaTopologyUtils.allowedToBeExported(apf001))
    assert(KafkaTopologyUtils.allowedToBeExported(noapf))
    assert(!KafkaTopologyUtils.allowedToBeExported(definedInstitution))
  }
}