/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.memobase.rico2edm.edm

import ch.memobase.rico2edm.utils.Helper
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import java.util.Properties

class HelperSpec extends AnyFunSuite with Matchers {


  //has to run as first test
  test("no initialization of mapping") {
    assert(Helper.getLanguageCode("http://www.wikidata.org/entity/Q27683").isEmpty)
  }


  test("load language iso codes") {
    Helper.initLanguageCodeMapping("src/test/resources/enrichement/few-language-codes.csv")
    assert(Helper.getLanguageCode("http://www.wikidata.org/entity/Q27683").get == "ace")
    //id is not available
    assert(Helper.getLanguageCode("http://www.wikidata.org/entity/Q2768").isEmpty)
  }


}
