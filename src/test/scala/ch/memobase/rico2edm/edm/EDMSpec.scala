/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.Utils
import ch.memobase.Utils.{loadFile, parseRecordFile}
import ch.memobase.rico2edm.edm.subjects.Aggregation
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.util.Try

class EDMSpec extends AnyFunSuite with Matchers {



  private lazy val ricoWithContributors = parseRecordFile(
    "src/test/resources/raw.contributor.json"
  )
  private lazy val rawEdmRights = loadFile(
    "src/test/resources/rico.certainty.json"
  )

  ignore("complete EDM creation") {
    val edm = new EDM

    val edmCreationResult: Try[ExtractionResult[(String, String)]] =
      edm.create(rawEdmRights, TestSettings)
    assert(edmCreationResult.isSuccess)
  }

  test("aggregation creation") {
    val agg = new Aggregation(ricoWithContributors.mainIdentifier, TestSettings)
    assert(
      Utils
        .serializeRDFModel(agg.getModel)
        .contains(
          """<ore:Aggregation rdf:about="https://memobase.ch/object/rts-002-FNB023_1639_023">"""
        )
    )
  }

}
