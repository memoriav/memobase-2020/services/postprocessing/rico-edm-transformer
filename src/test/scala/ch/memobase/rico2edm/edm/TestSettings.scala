/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.rico2edm.Settings

import java.util.Properties

object TestSettings extends Settings {
  override val kafkaInputTopic: String = "test-in"
  override val kafkaOutputTopic: String = "test-out"
  override val kafkaReportTopic: String = "test-report"
  override val reportingStepName: String = "test-step"
  override val recordSetIndex: String = "test-record-set-index"
  override val institutionIndex: String = "test-institution-index"
  override val esCluster: String = "test-cluster"
  override val esHosts: String = "localhost:9092"
  override val esApiKeyId: String = "abc"
  override val esApiKeySecret: String = "xyz"
  override val esCaCertPath: String = "/a/b/C"
  override val exportDefinitionRulesPath: String = "/a/b/c"
  override val institutionCoordinatesPath: String = "/a/b/c"
  override val isocodeMappingPath: String = "/a/b/c"
  override val memobaseFrontendUri: String = "https://memobase.ch"
  override val memobaseMediaServerUri: String = "localhost:9090"
  override val kafkaStreamsSettings: Properties = Properties()
}
