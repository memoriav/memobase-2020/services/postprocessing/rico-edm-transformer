/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.Utils
import ch.memobase.Utils.loadFile
import ch.memobase.rico.{FotoType, Record}
import ch.memobase.rico2edm.edm.subjects.Aggregation
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import java.net.URI

class AggregationSpec extends AnyFunSuite with Matchers {

  private lazy val rawRicoIndent = loadFile("src/test/resources/rawIndent.json")
  private lazy val rawCertainty = loadFile(
    "src/test/resources/rico.certainty.json"
  )
  private lazy val rawNoFoto = loadFile("src/test/resources/rico.duration.json")
  private lazy val rawEdmRights = loadFile(
    "src/test/resources/rico.certainty.json"
  )

  private lazy val rawTestedmObject = loadFile(
    "src/test/resources/apf-001-1280.json"
  )

  private def testAggregation = {
    val record = Record(rawRicoIndent).get
    Aggregation(record.mainIdentifier, TestSettings)
  }

  test("edm:aggregatedCHO") {
    assert(Record(rawCertainty).get.mainIdentifier == "lmz-001--597216")
  }

  test("edm:isShownBy") {
    assert(
      Record(
        rawCertainty
      ).get.digitalObject.get.mainIdentifier == "lmz-001--597216-1"
    )

  }

  private def createAgg(rawGraph: String): Aggregation = {
    val record = Record(rawGraph).get
    val digiObjIdShort = record.digitalObject.get.mainIdentifier
    val tempTestAgg = testAggregation

    if (record.ricoType == FotoType) {
      tempTestAgg.addEDMObjectFoto(digiObjIdShort)
    } else {
      tempTestAgg.addEDMObjectNoFoto(digiObjIdShort)
    }
    tempTestAgg
  }

  ignore("last test for silvia") {
    val source = loadFile("src/test/resources/apf-001-1284.edm.object.json")
    val edm = new EDM
    val agg = edm.createAggregation(Record(source).get, TestSettings).get.obj
    val cho = edm.createCulturalHeritageObject(Record(source).get).obj
    cho.getModel.addAll(agg.getModel)
    println(Utils.serializeRDFModel(cho.getModel))
  }

  //actually we can't run this test until we can use memobase.ch object links
  ignore("aggregation edm:object") {

    val aggFoto = createAgg(rawCertainty)
    assert(
      Utils
        .serializeRDFModel(aggFoto.getModel)
        .contains(
          s"<edm:object>https://media.memobase.ch/memo/lmz-001--597216-1/master</edm:object>"
        )
    )

    val aggNoFoto = createAgg(rawNoFoto)
    assert(
      Utils
        .serializeRDFModel(aggNoFoto.getModel)
        .contains(
          s"<edm:object>https://media.memobase.ch/memo/raf-001-2893-1-poster</edm:object>"
        )
    )

    //warum bekomme ich einen anderen Werz in der API
    //val errorCase = createAgg(rawTestedmObject)
    //println(Utils.serializeRDFModel(errorCase.getModel))

    val edm = new EDM
    val agg = edm.createAggregation(Record(rawTestedmObject).get, TestSettings)
    assert(
      Utils
        .serializeRDFModel(agg.get.obj.getModel)
        .contains(
          "<edm:object>https://media.memobase.ch/memo/apf-001-1280-1-poster</edm:object>"
        )
    )

  }

  test("edm:rights property for aggregation ") {
    val usage =
      Record(rawEdmRights).get.digitalObject.get.usageRegulatedBy.sameAs.get
    assert(usage == URI.create("http://rightsstatements.org/vocab/CNE/1.0/"))
  }
}
