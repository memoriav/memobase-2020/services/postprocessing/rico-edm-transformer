/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.Utils
import ch.memobase.Utils.parseRecordFile
import ch.memobase.rico2edm.edm.subjects.ProvidedCHO
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class CHOSpec extends AnyFunSuite with Matchers {

  private lazy val ricoWithContributors = parseRecordFile(
    "src/test/resources/raw.contributor.json"
  )
  private lazy val ricoContributorP60441 = parseRecordFile(
    "src/test/resources/contributor.p60441.json"
  )
  private lazy val ricoPlaceCapture = parseRecordFile(
    "src/test/resources/apf-001-1284.edm.object.json"
  )

  private lazy val ricoDuration = parseRecordFile(
    "src/test/resources/rico.duration.json"
  )

  private lazy val ricoRawContributor = parseRecordFile(
    "src/test/resources/raw.contributor.json"
  )

  private lazy val interactivetesting = parseRecordFile(
    "src/test/resources/wikidata.id.json"
  )

  ignore("only for interactive testing") {
    val cho = new EDM
    val model = cho
      .createCulturalHeritageObject(interactivetesting)
      .obj
      .getModel
    println(Utils.serializeRDFModel(model))
  }

  test("create CHOObject with Identifier") {
    val record = ricoDuration

    assert(record.rdfResourceIri == "https://memobase.ch/record/raf-001-2893")
    assert(record.mainIdentifier == "raf-001-2893")

    /*
    so far I haven't seen a possibility with rdf4j to create a resource as part of a model without an explicit base iri
    or to use an implicit base uri not part of the iri(...) argument
    by now I have to use an absolute iri

    If you are using a rio parser (Rio.parse) you can provide a base Uri as part of the constructor
    at the moment I haven't enough time to analyze it in more depth

     */
    assertThrows[IllegalArgumentException](
      new ProvidedCHO(record.mainIdentifier)
    )
    val cho = new ProvidedCHO(record.rdfResourceIri)
    assert(
      Utils
        .serializeRDFModel(cho.getModel)
        .contains(
          """<edm:ProvidedCHO rdf:about="https://memobase.ch/record/raf-001-2893">"""
        )
    )
  }

  test("get contributors") {
    val results = List(ricoWithContributors, ricoContributorP60441).map {
      record =>
        (
          record.resourceCreators.filter(_.roleType == "contributor"),
          record.resourceCreators.filter(_.roleType == "creator"),
          Utils.serializeRDFModel(
            (new EDM).createCulturalHeritageObject(record).obj.getModel
          )
        )
    }

    assert(results.head._1.length == 4)
    assert(results.head._2.length == 1)

    assert(
      results.head._3.contains(
        """<dc:creator>Studio Radio Lausanne</dc:creator>"""
      )
    )
    assert(
      results.head._3.contains("""<dc:contributor>inconnu</dc:contributor>""")
    )
    assert(
      results.head._3.contains(
        """<dc:contributor>COTY, René</dc:contributor>"""
      )
    )
    assert(
      results.head._3.contains(
        """<dc:contributor>PFLIMLIN, Pierre</dc:contributor>"""
      )
    )
    assert(
      results.head._3.contains(
        """<dc:contributor>Studio Radio Lausanne</dc:contributor>"""
      )
    )

    assert(results(1)._1.length == 2)
    assert(results(1)._2.length == 1)
    assert(ricoContributorP60441.producers.head.name == "Studio Radio Lausanne")
    assert(
      results(1)._3.contains(
        """<dc:creator>Studio Radio Lausanne</dc:creator>"""
      )
    )
    assert(
      results(1)._3.contains("""<dc:contributor>inconnu</dc:contributor>""")
    )
    assert(
      results(1)._3.contains(
        """<dc:contributor>RIGASSI, Vico</dc:contributor>"""
      )
    )
    assert(
      results(1)._3.contains(
        """<dc:contributor>Studio Radio Lausanne</dc:contributor>"""
      )
    )
  }

  test("collect genre dc:type") {
    val dcType = Utils.serializeRDFModel(
      (new EDM).createCulturalHeritageObject(ricoRawContributor).obj.getModel
    )
    assert(dcType.contains("<dc:type>Intervista</dc:type>"))
    assert(dcType.contains("<dc:type>Parlato</dc:type>"))
    assert(dcType.contains("<dc:type>Rede</dc:type>"))
    assert(dcType.contains("<dc:type>Gesprochen</dc:type>"))
    assert(dcType.contains("<dc:type>Interview</dc:type>"))
    assert(dcType.contains("<dc:type>Parlé</dc:type>"))
    assert(dcType.contains("<dc:type>Discours</dc:type>"))
    assert(dcType.contains("<dc:type>Discorso</dc:type>"))
  }

  //we can't run this test because we use test links not memobase.ch
  ignore("cho") {
    val cho = Utils.serializeRDFModel(
      (new EDM).createCulturalHeritageObject(ricoPlaceCapture).obj.getModel
    )
    assert(cho.contains("<dc:type>entretien</dc:type>"))
    assert(
      cho.contains(
        "<dcterms:medium>http://www.wikidata.org/entity/Q875215</dcterms:medium>"
      )
    )
    assert(cho.contains("<dc:subject>Benno Besson</dc:subject>"))
  }

  ignore("collect dcterms:medium") {
    val serialization = Utils.serializeRDFModel(
      (new EDM).createCulturalHeritageObject(ricoRawContributor).obj.getModel
    )
    assert(
      serialization.contains("<dcterms:medium>Direktschnitt</dcterms:medium>")
    )
    assert(
      serialization.contains(
        "<dcterms:medium>L'ÉTIQUETTE MANQUANTE</dcterms:medium>"
      )
    )
    assert(
      serialization.contains(
        "<dcterms:medium>GALATEO MANCANTE</dcterms:medium>"
      )
    )
  }

  test("record hasOrHadHolder") {
    val test = ricoDuration.holders.map(EDM.getInstitutionOrRecordsetIdent)
    println(test)
  }

}
