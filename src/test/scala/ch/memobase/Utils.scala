/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.rico.Record
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.rio.Rio
import ch.memobase.rico2edm.rdf.writer.{RdfXmlWriter => EDMRdfXmlWriter}

import java.io.StringWriter
import scala.io.Source

object Utils {
  def loadFile(uri: String): String = {
    val source = Source.fromFile(uri)
    val string = source.getLines().mkString("\n")
    source.close
    string
  }

  def parseRecordFile(path: String): Record = {
    Record(loadFile(path)).get
  }

  def serializeRDFModel(model: Model): String = {

    val sOut = new StringWriter
    val rdfWriter = new EDMRdfXmlWriter(sOut)
    Rio.write(model, rdfWriter)
    sOut.toString

  }
}
