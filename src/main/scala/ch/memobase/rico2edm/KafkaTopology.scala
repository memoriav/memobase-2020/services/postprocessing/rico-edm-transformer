/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm

import ch.memobase.rico2edm.edm.{EDM, ExtractionResult}
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Named
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.kstream.{Branched, KStream}
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.logging.log4j.scala.Logging

import scala.util.Try

class KafkaTopology extends Logging {

  import KafkaTopologyUtils._
  import org.apache.kafka.streams.scala.serialization.Serdes._

  def build(
      topicIn: String,
      topicOut: String,
      reportingTopic: String,
      settings: Settings
  ): Topology = {

    val builder = new StreamsBuilder
    val edmBuilder = new EDM

    val source = builder
      .stream[String, String](topicIn)
      .filter((key, value) =>
        value == null && {
          logger.info(s"Ignored message with key '$key' as value is null.")
          true
        }
      )

    //val Array(isEDMDeliverable, noOaiExport) = source
    val oaiExport = source
      .split(Named.as("oai-export-"))
      .branch((_, v) => allowedToBeExported(v), Branched.as("true"))
      .defaultBranch(Branched.as("false"))

    //noinspection ConvertibleToMethodValue
    val edmModel = oaiExport.get("oai-export-false")
      .map(_.mapValues(v => edmBuilder.create(v, settings))
        .split(Named.as("model-creation-"))
        .branch((_, v) => v.isSuccess && v.get.warnings.nonEmpty, Branched.as("warnings"))
        .branch((_, v) => v.isSuccess, Branched.as("success"))
        .defaultBranch(Branched.as("failure"))
      )
      .getOrElse(Map())

    //val Array(hasWarnings, isDeliverable, noEDM) = isEDMDeliverable

    edmModel.get("model-creation-warnings").foreach(sendRecord(_, topicOut))
    edmModel.get("model-creation-success").foreach(sendRecord(_, topicOut))

    edmModel.get("model-creation-warnings").foreach(reportManifestCreationWarnings(_, reportingTopic))
    edmModel.get("model-creation-success").foreach(reportSuccessfulEDMCreation(_, reportingTopic))
    edmModel.get("model-creation-failure").foreach(reportEDMCreationFailure(_, reportingTopic))

    oaiExport
      .get("oai-export-false")
      .foreach(
        reportIgnoredRecord(_, reportingTopic, "record barred from oai export")
      )
    builder.build()
  }

  private def sendRecord(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicOut: String
  ): Unit = {
    stream
      .map((_, v) => (extractId(v.get.obj._1), v.get.obj._2))
      .to(topicOut)
  }

  private def reportManifestCreationWarnings(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String
  ): Unit =
    stream
      .map((k, v) =>
        (
          k,
          ReportingObject(
            k,
            ProcessingWarning,
            v.get.warnings.mkString("\n")
          ).toString
        )
      )
      .to(topicReport)

  private def reportSuccessfulEDMCreation(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String
  ): Unit =
    stream
      .map((k, _) =>
        (
          k,
          ReportingObject(
            k,
            ProcessingSuccess,
            "EDM document for Europeana successfully created"
          ).toString
        )
      )
      .to(topicReport)

  private def reportEDMCreationFailure(
      stream: KStream[String, Try[ExtractionResult[(String, String)]]],
      topicReport: String
  ): Unit =
    stream
      .map((k, v) =>
        (
          k,
          ReportingObject(
            k,
            ProcessingFatal,
            s"Error creating EDM document: ${v.failed.get.getMessage}"
          ).toString
        )
      )
      .to(topicReport)

  private def reportIgnoredRecord(
      stream: KStream[String, String],
      topicReport: String,
      message: String
  ): Unit =
    stream
      .map((k, _) =>
        (
          k,
          ReportingObject(
            k,
            ProcessingIgnore,
            message
          ).toString
        )
      )
      .to(topicReport)
}
