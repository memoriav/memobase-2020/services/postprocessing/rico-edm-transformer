/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm

import ch.memobase.rico.Record
import ch.memobase.rico2edm.AppSettings.exportDefinitionRulesPath
import ch.memobase.rico2edm.utils.Helper

case class OaiExportRules(institutions: List[String], dataSets: List[String])

object KafkaTopologyUtils {

  private val entityWhitelist =
    Helper.initOaiExportRules(exportDefinitionRulesPath)

  def extractId(uri: String): String =
    uri.split("/").last.split("\\.(?=[^.]+$)")(0)

  def allowedToBeExported(msgVal: String): Boolean = {
    Record(msgVal)
      .map { record =>
        entityWhitelist.institutions.toSet
          .intersect(record.holders.toSet)
          .nonEmpty || entityWhitelist.recordSets.contains(
          record.partOf
        ) || entityWhitelist.documents.contains(record.mainIdentifier)
      }
      .getOrElse(false)

  }
}
