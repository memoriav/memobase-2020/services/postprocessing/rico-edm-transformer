/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.rdf.vocabularies

import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.impl.SimpleValueFactory

sealed abstract class VocabularyFactory(val NAMESPACE: String) {
  def getIri(localName: String): IRI =
    SimpleValueFactory.getInstance().createIRI(NAMESPACE, localName)
}

object EDMVocab
    extends VocabularyFactory("http://www.europeana.eu/schemas/edm/") {
  val ProvidedCHO: IRI = getIri("ProvidedCHO")
  val WebResource: IRI = getIri("WebResource")
  val TYPE: IRI = getIri("type")
  val RIGHTS: IRI = getIri("rights")
  val AGGREGATED_CHO: IRI = getIri("aggregatedCHO")
  val IS_SHOWN_AT: IRI = getIri("isShownAt")
  val IS_SHOWN_BY: IRI = getIri("isShownBy")
  val OBJECT: IRI = getIri("object")
  val PROVIDER: IRI = getIri("provider")
  val CURRENT_LOCATION: IRI = getIri("currentLocation")
  val DATA_PROVIDER: IRI = getIri("dataProvider")
}

object OreVocab
    extends VocabularyFactory("http://www.openarchives.org/ore/terms/") {
  val AGGREGATION: IRI = getIri("Aggregation")
}
