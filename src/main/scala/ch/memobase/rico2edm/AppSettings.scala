/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm

import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.processor.WallclockTimestampExtractor

import java.util.Properties
import org.apache.kafka.common.config.SslConfigs

trait Settings {
  val kafkaInputTopic: String
  val kafkaOutputTopic: String
  val kafkaReportTopic: String
  val reportingStepName: String
  val recordSetIndex: String
  val institutionIndex: String
  val esCluster: String
  val esHosts: String
  val esApiKeyId: String
  val esApiKeySecret: String
  val esCaCertPath: String
  val exportDefinitionRulesPath: String
  val institutionCoordinatesPath: String
  val isocodeMappingPath: String
  val memobaseFrontendUri: String
  val memobaseMediaServerUri: String
  val kafkaStreamsSettings: Properties
}

object AppSettings extends Settings {
  val kafkaInputTopic: String = sys.env("TOPIC_IN")
  val kafkaOutputTopic: String = sys.env("TOPIC_OUT")
  val kafkaReportTopic: String = sys.env("TOPIC_REPORT")
  val reportingStepName: String = sys.env("REPORTING_STEP_NAME")
  val recordSetIndex: String = sys.env("RECORD_SET_INDEX")
  val institutionIndex: String = sys.env("INSTITUTION_INDEX")
  val esCluster: String = sys.env("ELASTIC_CLUSTER_NAME")
  val esHosts: String = sys.env("ELASTIC_HOST_LIST")
  val esApiKeyId: String = sys.env("ELASTIC_API_KEY_ID")
  val esApiKeySecret: String = sys.env("ELASTIC_API_KEY_SECRET")
  val esCaCertPath: String = sys.env("ELASTIC_CA_CERT_PATH")
  val exportDefinitionRulesPath: String =
    sys.env("EXPORT_DEFINITION_RULES_PATH")
  val institutionCoordinatesPath: String =
    sys.env("INSTITUTION_COORDINATES_PATH")
  val isocodeMappingPath: String = sys.env("ISOCODE_MAPPING_PATH")
  val memobaseFrontendUri: String = "https://memobase.ch"
  val memobaseMediaServerUri: String = "https://media.memobase.ch"

  val kafkaStreamsSettings: Properties = {
    val properties = new Properties()
    List(
      SslConfigs.SSL_KEYSTORE_TYPE_CONFIG,
      SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG,
      SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_KEY_PASSWORD_CONFIG,
      SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG,
      SslConfigs.SSL_KEYSTORE_KEY_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG,
      StreamsConfig.APPLICATION_ID_CONFIG,
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
      StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
      StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
      StreamsConfig.APPLICATION_SERVER_CONFIG,
      StreamsConfig.ADMIN_CLIENT_PREFIX,
      StreamsConfig.AT_LEAST_ONCE,
      StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG,
      StreamsConfig.CLIENT_ID_CONFIG,
      StreamsConfig.COMMIT_INTERVAL_MS_CONFIG,
      StreamsConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG,
      StreamsConfig.CONSUMER_PREFIX,
      StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
      StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG,
      StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG,
      StreamsConfig.GLOBAL_CONSUMER_PREFIX,
      StreamsConfig.METRIC_REPORTER_CLASSES_CONFIG,
      StreamsConfig.METRICS_NUM_SAMPLES_CONFIG,
      StreamsConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG,
      StreamsConfig.MAIN_CONSUMER_PREFIX,
      StreamsConfig.MAX_TASK_IDLE_MS_CONFIG,
      StreamsConfig.METADATA_MAX_AGE_CONFIG,
      StreamsConfig.METRICS_RECORDING_LEVEL_CONFIG,
      StreamsConfig.NO_OPTIMIZATION,
      StreamsConfig.NUM_STANDBY_REPLICAS_CONFIG,
      StreamsConfig.NUM_STREAM_THREADS_CONFIG,
      StreamsConfig.OPTIMIZE,
      StreamsConfig.POLL_MS_CONFIG,
      StreamsConfig.PROCESSING_GUARANTEE_CONFIG,
      StreamsConfig.PRODUCER_PREFIX,
      StreamsConfig.RECEIVE_BUFFER_CONFIG,
      StreamsConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG,
      StreamsConfig.RECONNECT_BACKOFF_MS_CONFIG,
      StreamsConfig.REPLICATION_FACTOR_CONFIG,
      StreamsConfig.REQUEST_TIMEOUT_MS_CONFIG,
      StreamsConfig.RESTORE_CONSUMER_PREFIX,
      StreamsConfig.RETRY_BACKOFF_MS_CONFIG,
      StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG,
      StreamsConfig.SECURITY_PROTOCOL_CONFIG,
      StreamsConfig.SEND_BUFFER_CONFIG,
      StreamsConfig.STATE_CLEANUP_DELAY_MS_CONFIG,
      StreamsConfig.STATE_DIR_CONFIG,
      StreamsConfig.TOPIC_PREFIX,
      StreamsConfig.WINDOW_STORE_CHANGE_LOG_ADDITIONAL_RETENTION_MS_CONFIG
    )
      .map(p => (p, p.replaceAll(raw"\.", "_").toUpperCase))
      .collect {
        case p if sys.env.contains(p._2) => (p._1, p._2)
      }
      .foreach(p => {
        properties.setProperty(p._1, sys.env(p._2))
      })
    // use processingTime for newly created messages in topic otherwise messages could be immediately deleted
    properties.put(
      StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG,
      classOf[WallclockTimestampExtractor]
    )
    properties
  }
}
