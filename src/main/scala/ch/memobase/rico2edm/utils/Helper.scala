/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.utils

import java.io.ByteArrayOutputStream
import java.net.URI
import java.util.zip.Deflater
import java.util.{HashMap => JHashMap}
import scala.language.reflectiveCalls

case class EntityWhitelist(
    institutions: List[String] = List.empty,
    recordSets: List[String] = List.empty,
    documents: List[String] = List.empty
)

object Helper {

  private var isoLanguageCodes: Option[JHashMap[String, String]] = None

  private var institutionsCoordinates: Option[JHashMap[String, String]] = None

  def compress(data: Array[Byte]): Array[Byte] = {
    val deflater = new Deflater()
    deflater.setInput(data)
    val outputStream = new ByteArrayOutputStream(data.length)
    deflater.finish()
    val buffer = new Array[Byte](1024)
    while ({
      !deflater.finished
    }) {
      val count =
        deflater.deflate(buffer)
      outputStream.write(buffer, 0, count)
    }
    outputStream.close()
    outputStream.toByteArray
  }

  def initOaiExportRules(exportDefinitionRulesPath: String): EntityWhitelist = {
    val institutionPattern = """^\w$""".r
    val recordSetPattern = """^\w-\d$""".r
    val documentPattern = """^\w-\d-.+$""".r
    using(scala.io.Source.fromFile(exportDefinitionRulesPath)) { source =>
      source.getLines().foldRight(EntityWhitelist())((id, agg) =>
        id match {
          case institutionPattern(_) =>
            agg.copy(institutions = agg.institutions :+ id)
          case recordSetPattern(_) =>
            agg.copy(recordSets = agg.recordSets :+ id)
          case documentPattern(_) => agg.copy(documents = agg.documents :+ id)
          case _                  => agg
        }
      )
    }
  }

  def initLanguageCodeMapping(isocodeMappingPath: String): Unit = {
    val isoCodes = new JHashMap[String, String]()
    using(scala.io.Source.fromFile(isocodeMappingPath)) { source =>
      for (line <- source.getLines()) {
        val temp = line.split(",").map(_.trim)
        isoCodes.put(temp(0), temp(1))
      }
    }
    isoLanguageCodes = Some(isoCodes)
  }

  def initInstitutionsCoordinateMapping(
      institutionCoordinatesPath: String
  ): Unit = {

    val coord = new JHashMap[String, String]()
    using(scala.io.Source.fromFile(institutionCoordinatesPath)) { source =>
      for (line <- source.getLines()) {
        val temp = line.split(",").map(_.trim)
        coord.put(temp(0), temp(1))
      }
    }
    institutionsCoordinates = Some(coord)
  }

  //noinspection ScalaStyle
  private def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }

  def getLanguageCode(wikiId: String): Option[String] = {
    isoLanguageCodes match {
      case Some(langkeys) if langkeys.containsKey(wikiId) =>
        Some(langkeys.get(wikiId))
      case _ => None
    }

  }

  def getInstitutionCoord(institutionId: String): Option[String] = {
    institutionsCoordinates match {
      case Some(coords) if coords.containsKey(institutionId) =>
        Some(coords.get(institutionId))
      case _ => None
    }

  }

}
