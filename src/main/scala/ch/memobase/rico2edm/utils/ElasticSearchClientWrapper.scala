/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.utils

import co.elastic.clients.elasticsearch.ElasticsearchClient
import co.elastic.clients.elasticsearch.core.GetRequest
import co.elastic.clients.json.jackson.JacksonJsonpMapper
import co.elastic.clients.transport.rest_client.RestClientTransport
import com.fasterxml.jackson.databind.node.ObjectNode
import org.apache.http.{Header, HttpHost}
import org.apache.http.message.BasicHeader
import org.apache.http.ssl.SSLContexts
import org.apache.logging.log4j.scala.Logging
import org.elasticsearch.client.{RequestOptions, RestClient}

import java.io.{File, FileInputStream}
import java.nio.charset.StandardCharsets
import java.security.KeyStore
import java.security.cert.CertificateFactory
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
import scala.jdk.CollectionConverters.*
import java.util.{Base64, ArrayList as JArrayList, HashMap as JHashMap}
import javax.net.ssl.SSLContext
import java.text.ParseException

private sealed trait Index
private case object RecordSetIndex extends Index
private case object InstitutionIndex extends Index

class ElasticSearchClientWrapper private (
    val client: ElasticsearchClient,
    val recordSetIndex: String,
    val institutionIndex: String
) {

  private type ESStringList = JHashMap[String, JArrayList[String]]

  private def getRecordSetOrInstitutionName(
      id: String,
      indexType: Index
  ): Try[String] =
    Try {
      val indexName = indexType match {
        case InstitutionIndex => institutionIndex
        case RecordSetIndex   => recordSetIndex
      }
      val getRequest = new GetRequest.Builder().index(indexName).id(id).build()
      val getResponse = client.get(getRequest, classOf[ESStringList])

      if (getResponse.source.isEmpty) {
        throw new Exception(
          s"${if (indexType == RecordSetIndex) "recordSet" else "institution"} with identifier $id not found "
        )
      } else {
        val hit = getResponse.source().asScala
        val institutionName = hit
          .getOrElse("name", new ESStringList())
          .asInstanceOf[ESStringList]
          .asScala
        institutionName.get("de").map(_.get(0)).getOrElse("unknown")
      }
    }

}

object ElasticSearchClientWrapper extends Logging {

  private var client: Option[ElasticSearchClientWrapper] = None

  def apply(
      esClusterName: String,
      esHosts: String,
      esApiKeyId: String,
      esApiKeySecret: String,
      esCaCertPath: String,
      recordSetIndex: String,
      institutionIndex: String
  ): Boolean =
    connect(
      esClusterName,
      esHosts,
      esApiKeyId,
      esApiKeySecret,
      esCaCertPath
    ) match {
      case Success(esClient) =>
        client = Option(
          new ElasticSearchClientWrapper(
            esClient,
            recordSetIndex,
            institutionIndex
          )
        )
        true
      case Failure(exception) =>
        logger.error(s"error initializing $exception")
        false
    }

  def getHeldBy(id: String): Option[String] =
    client.flatMap(_.getRecordSetOrInstitutionName(id, InstitutionIndex) match {
      case Success(instTitle) => Some(instTitle)
      case Failure(exception) =>
        logger.error(s"error trying to get institution title: $exception")
        None
    })

  def getRecordsetName(idRecordSet: String): Option[String] =
    client.flatMap(
      _.getRecordSetOrInstitutionName(idRecordSet, RecordSetIndex) match {
        case Success(instTitle) => Some(instTitle)
        case Failure(exception) =>
          logger.error(s"error trying to get recordset title: $exception")
          None
      }
    )

  private def connect(
      esClusterName: String,
      esHosts: String,
      esApiKeyId: String,
      esApiKeySecret: String,
      esCaCertPath: String
  ) =
    Try {
      logger.debug("Parsing hosts")
      val configuredHosts = parseHosts(esHosts).get
      logger.debug("Hosts parsed")
      val apiKeyAuth = Base64.getEncoder.encodeToString(
        s"$esApiKeyId:$esApiKeySecret"
          .getBytes(StandardCharsets.UTF_8)
      )
      val headers = Array(
        new BasicHeader("cluster.name", esClusterName)
          .asInstanceOf[Header],
        new BasicHeader("Authorization", s"ApiKey $apiKeyAuth")
          .asInstanceOf[Header]
      )

      logger.debug("Building SSL context")
      val sslContext = buildSSLContext(esCaCertPath).get
      logger.debug("SSL context built")
      val restClient =
        RestClient
          .builder(configuredHosts*)
          .setHttpClientConfigCallback(httpClientBuilder =>
            httpClientBuilder.setSSLContext(sslContext)
          )
          .setDefaultHeaders(headers)
          .build()

      val transport = new RestClientTransport(
        restClient,
        new JacksonJsonpMapper()
      )
      new ElasticsearchClient(transport)
    }

  private def buildSSLContext(caCertPath: String): Try[SSLContext] = {
    Try {
      val factory = CertificateFactory.getInstance("X.509")
      val trustedCa =
        factory.generateCertificate(new FileInputStream(new File(caCertPath)))
      val trustStore = KeyStore.getInstance("pkcs12")
      trustStore.load(null, null)
      trustStore.setCertificateEntry("ca", trustedCa)
      SSLContexts.custom.loadTrustMaterial(trustStore, null).build
    }
  }

  private val hostRegex = """^(https?)://(.*):(\d+)$""".r

  private def parseHosts(hosts: String): Try[Array[HttpHost]] = Try {
    hosts
      .split(',')
      .map(h => {
        val hostParts = hostRegex
          .findFirstMatchIn(h)
          .getOrElse(
            throw new RuntimeException(
              "Elastic host URLs must comply to pattern `^(https?)://(.*):(\\d+)$`"
            )
          )
        val scheme = hostParts.group(1)
        val hostName = hostParts.group(2)
        val port = hostParts.group(3).toInt
        new HttpHost(hostName, port, scheme)
      })
  }
}
