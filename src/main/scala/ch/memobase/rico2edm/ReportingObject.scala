/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm

import ch.memobase.rico2edm.ReportingObject.createTimestamp
import AppSettings._

import java.text.SimpleDateFormat
import java.util.Calendar

sealed trait ProcessingStatus {
  val value: String
}

case object ProcessingSuccess extends ProcessingStatus {
  override val value: String = "SUCCESS"
}

case object ProcessingWarning extends ProcessingStatus {
  override val value: String = "WARNING"
}

case object ProcessingIgnore extends ProcessingStatus {
  override val value: String = "IGNORE"
}

case object ProcessingFatal extends ProcessingStatus {
  override val value: String = "FATAL"
}

case class ReportingObject(
    id: String,
    status: ProcessingStatus,
    message: String
) {
  override def toString: String =
    ujson.write(
      ujson.Obj(
        ("step", reportingStepName),
        ("stepVersion", getClass.getPackage.getImplementationVersion),
        ("timestamp", createTimestamp),
        ("id", id),
        ("status", status.value),
        ("message", message)
      )
    )
}

object ReportingObject {
  private val dateFormatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS")

  def createTimestamp: String =
    dateFormatter.format(Calendar.getInstance().getTime)
}
