/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm.subjects

import ch.memobase.rico2edm.Settings
import ch.memobase.rico2edm.rdf.vocabularies.{EDMVocab, OreVocab}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.util.Values.iri
import org.eclipse.rdf4j.model.vocabulary.{DC, DCTERMS, RDF}

class ProvidedCHO(val id: String) {

  import org.eclipse.rdf4j.model.util.Values.iri

  private val model = EDMRdf4jModel.getModelWithEDMNamespaces
  model.add(iri(id), RDF.TYPE, EDMVocab.ProvidedCHO)

  private val factory = SimpleValueFactory.getInstance()

  def addDescription(description: String): Unit =
    model.add(iri(id), DC.DESCRIPTION, factory.createLiteral(description))

  def addTitle(title: Option[String]): Unit = {
    title.map(t => model.add(iri(id), DC.TITLE, factory.createLiteral(t)))
  }

  def addCreator(creator: String): Unit =
    model.add(iri(id), DC.CREATOR, factory.createLiteral(creator))

  def addContributor(contributor: String): Unit =
    model.add(iri(id), DC.CONTRIBUTOR, factory.createLiteral(contributor))

  def addIdentifier(identifier: String): Unit =
    model.add(iri(id), DC.IDENTIFIER, factory.createLiteral(identifier))

  def addLanguage(language: String): Unit =
    model.add(iri(id), DC.LANGUAGE, factory.createLiteral(language))

  def addPublisher(publisher: String): Unit =
    model.add(iri(id), DC.PUBLISHER, factory.createLiteral(publisher))

  def addRight(right: String): Unit =
    model.add(iri(id), DC.RIGHTS, factory.createLiteral(right))

  def addSource(source: String): Unit =
    model.add(iri(id), DC.SOURCE, factory.createLiteral(source))

  def addSubject(subject: String): Unit =
    model.add(iri(id), DC.SUBJECT, factory.createLiteral(subject))

  def addGenre(genre: String): Unit =
    model.add(iri(id), DC.TYPE, factory.createLiteral(genre))

  def addDcTermsCreated(dctermsCreated: String): Unit =
    model.add(iri(id), DCTERMS.CREATED, factory.createLiteral(dctermsCreated))

  def addDcTermsExtent(duration: String): Unit =
    model.add(iri(id), DCTERMS.EXTENT, factory.createLiteral(duration))

  def addDcTermsIssued(issuedDate: String): Unit =
    model.add(iri(id), DCTERMS.ISSUED, factory.createLiteral(issuedDate))

  def addDcTermsMedium(medium: String): Unit =
    model.add(iri(id), DCTERMS.MEDIUM, factory.createIRI(medium))

  def addDcTermsSpatial(placeName: String): Unit =
    model.add(iri(id), DCTERMS.SPATIAL, factory.createLiteral(placeName))

  def addEdmType(edmType: String): Unit =
    model.add(iri(id), EDMVocab.TYPE, factory.createLiteral(edmType))

  def addCurrentLocation(edmCurrentLocation: String): Unit =
    model.add(
      iri(id),
      EDMVocab.CURRENT_LOCATION,
      factory.createIRI(edmCurrentLocation)
    )

  def addIsPartOf(dcTermsPartOf: String): Unit =
    model.add(iri(id), DCTERMS.IS_PART_OF, factory.createLiteral(dcTermsPartOf))

  def getModel: Model = model

}

class WebResource(private val id: String) {
  private val model = EDMRdf4jModel.getModelWithEDMNamespaces
  private val factory = SimpleValueFactory.getInstance()

  def addRights(edmRights: Option[String]): Unit =
    edmRights.map(t =>
      model.add(iri(id), EDMVocab.RIGHTS, factory.createIRI(t))
    )

  def addDcTermsExtent(dctermsExtent: Option[String]): Unit =
    dctermsExtent.map(t =>
      model.add(iri(id), DCTERMS.EXTENT, factory.createLiteral(t))
    )

  def addDcFormat(dcFormat: Option[String]): Unit =
    dcFormat.map(t => model.add(iri(id), DC.FORMAT, factory.createLiteral(t)))

  model.add(iri(id), RDF.TYPE, EDMVocab.WebResource)

  def getModel: Model = model

}

object WebResource {
  private val identValue = "^https.*".r

  def apply(shortID: String, settings: Settings): WebResource =
    if (identValue.matches(shortID)) {
      new WebResource(shortID)
    } else {
      new WebResource(s"${settings.memobaseMediaServerUri}/memo/$shortID/master")
    }
}

class Aggregation(private val shortId: String, settings: Settings) {

  private val id = s"${settings.memobaseFrontendUri}/object/$shortId"
  private val model = EDMRdf4jModel.getModelWithEDMNamespaces
  model.add(iri(id), RDF.TYPE, OreVocab.AGGREGATION)

  private val factory = SimpleValueFactory.getInstance()

  def addAggregatedCHO(edmAggregatedCHO: String): Unit =
    model.add(
      iri(id),
      EDMVocab.AGGREGATED_CHO,
      factory.createIRI(edmAggregatedCHO)
    )

  def addIsShownAt(edmIsShownAt: String): Unit =
    if (edmIsShownAt.startsWith("https")) {
      model.add(iri(id), EDMVocab.IS_SHOWN_AT, factory.createIRI(edmIsShownAt))
    } else {
      model.add(
        iri(id),
        EDMVocab.IS_SHOWN_AT,
        factory.createIRI(s"${settings.memobaseFrontendUri}/object/$edmIsShownAt")
      )
    }

  def addIsShownBy(edmIsShownBy: String): Unit =
    if (edmIsShownBy.startsWith("https")) {
      model.add(iri(id), EDMVocab.IS_SHOWN_BY, factory.createIRI(edmIsShownBy))
    } else {
      model.add(
        iri(id),
        EDMVocab.IS_SHOWN_BY,
        factory.createIRI(s"${settings.memobaseMediaServerUri}/memo/$edmIsShownBy/master")
      )
    }

  def addEDMObjectFoto(edmObjectFoto: String): Unit =
    if (edmObjectFoto.startsWith("https")) {
      model.add(iri(id), EDMVocab.OBJECT, factory.createIRI(edmObjectFoto))
    } else {
      model.add(
        iri(id),
        EDMVocab.OBJECT,
        factory.createIRI(s"${settings.memobaseMediaServerUri}/memo/$edmObjectFoto/master")
      )
    }

  def addProvider(edmProvider: String): Unit =
    model.add(iri(id), EDMVocab.PROVIDER, factory.createLiteral(edmProvider))

  def addDataProvider(edmDataProvider: String): Unit =
    model.add(
      iri(id),
      EDMVocab.DATA_PROVIDER,
      factory.createLiteral(edmDataProvider)
    )

  def addEDMObjectNoFoto(edmObjectNoFoto: String): Unit =
    if (edmObjectNoFoto.startsWith("https")) {
      model.add(iri(id), EDMVocab.OBJECT, factory.createIRI(edmObjectNoFoto))
    } else {
      model.add(
        iri(id),
        EDMVocab.OBJECT,
        factory.createIRI(
          s"${settings.memobaseMediaServerUri}/memo/$edmObjectNoFoto-poster"
        )
      )
    }

  def addRights(edmRights: String): Unit =
    model.add(iri(id), EDMVocab.RIGHTS, factory.createIRI(edmRights))

  def getModel: Model = model

}

object Aggregation {

  def apply(shortID: String, settings: Settings): Aggregation = new Aggregation(shortID, settings)

}
object EDMRdf4jModel {
  def getModelWithEDMNamespaces: Model = {
    val builder = new ModelBuilder
    builder
      .setNamespace("dc", "http://purl.org/dc/elements/1.1/")
      .setNamespace("dcterms", "http://purl.org/dc/terms/")
      .setNamespace("edm", "http://www.europeana.eu/schemas/edm/")
      .setNamespace("ore", "http://www.openarchives.org/ore/terms/")
      .setNamespace("owl", "http://www.w3.org/2002/07/owl#")
      .setNamespace("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
      .setNamespace("foaf", "http://xmlns.com/foaf/0.1/")
      .setNamespace("wgs84_pos", "http://www.w3.org/2004/02/skos/core#")
      .setNamespace("crm", "http://www.cidoc-crm.org/cidoc-crm/")
      .setNamespace("cc", "http://creativecommons.org/ns#")
      .build()
  }
}
