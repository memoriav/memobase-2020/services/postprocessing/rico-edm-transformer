/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm.subjects

import ch.memobase.rico2edm.rdf.writer.{RdfXmlWriter => EDMRdfXmlWriter}
import ch.memobase.rico2edm.utils.Helper
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.rio.Rio
//import org.eclipse.rdf4j.rio.rdfxml.RDFXMLWriter

import java.io.StringWriter
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Base64

object ModelXMLTransformer {
  private val prologPattern = "<\\?xml.*?\\?>".r
  private val dateTimeFormatter = DateTimeFormatter.ISO_INSTANT

  def apply(
      model: Model,
      id: String,
      recordset: String,
      institution: String,
      published: Boolean = true,
      format: String = "edm"
  ): String = {

    val sOut = new StringWriter
    val rdfWriter = new EDMRdfXmlWriter(sOut)
    Rio.write(model, rdfWriter)

    //create whole ES structure and replace XML prolog
    //what about OAI header - I think we can create it here and should relief the OAI server from this task
    //println(sOut)

    prologPattern.replaceFirstIn(
      ujson
        .Obj(
          //is this the correct ID
          "id" -> id,
          "document" -> Base64.getEncoder
            .encodeToString(Helper.compress(sOut.toString.getBytes)),
          //"document" ->  sOut.toString,
          "format" -> format,
          //we need specific rules to decide which documents are going to be published
          //or we have to filter them out
          "published" -> published,
          //for the recordset and institution evaluate the information sent by Silvia
          //by now fixed values
          "recordset" -> recordset,
          "institution" -> institution,
          "lastUpdatedDate" -> dateTimeFormatter.format(ZonedDateTime.now)
        )
        .toString,
      ""
    )

  }

}
