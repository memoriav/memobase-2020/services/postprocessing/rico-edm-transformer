/*
 * rico2edm
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase.rico2edm.edm

import ch.memobase.rico.{FilmType, FotoType, RadioType, Record, TVType, TonType, TonbildschauType, VideoType, VimeoDistributor}
import ch.memobase.rico2edm.Settings
import ch.memobase.rico2edm.edm.subjects.*
import ch.memobase.rico2edm.utils.{ElasticSearchClientWrapper, Helper}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.Try

class EDM {

  def create(messageValue: String, settings: Settings): Try[ExtractionResult[(String, String)]] = {
    Try {
      val record: Record = Record(messageValue).get

      val choExtraction = createCulturalHeritageObject(record)
      val webExtraction = createWebResources(record, settings)

      val aggregationExtraction = createAggregation(record, settings)

      webExtraction.obj.foreach(webResource =>
        choExtraction.obj.getModel.addAll(webResource.getModel)
      )

      aggregationExtraction.map(agg =>
        choExtraction.obj.getModel.addAll(agg.obj.getModel)
      )

      val esObject = ModelXMLTransformer(
        model = choExtraction.obj.getModel,
        id = record.mainIdentifier,
        recordset = record.partOf,
        institution = record.holders.head
      )

      //todo: by now we do not collect any infos and warnings
      ExtractionResult(
        (
          record.mainIdentifier,
          esObject
        ),
        new ArrayBuffer[String]()
      )
    }
  }

  //scalastyle:off cyclomatic.complexity
  def createCulturalHeritageObject(
      record: Record
  ): ExtractionResult[ProvidedCHO] = {

    val cho = new ProvidedCHO(record.rdfResourceIri)

    val titles = record.titles
    if (titles.nonEmpty) {
      titles
        .foreach(c => cho.addTitle(Some(c.title)))
    } else {
      cho.addTitle(Some(record.mainTitle))
    }

    record.abstracts.foreach(summary => cho.addDescription(summary))
    record.scopesAndContents.foreach(scope => cho.addDescription(scope))
    record.descriptiveNotes.foreach(description =>
      cho.addDescription(description)
    )

    record.resourceCreators
      .filter(_.roleType == "creator")
      .foreach(creator => cho.addCreator(creator.agent.name))

    record.resourceCreators
      .filter(_.roleType == "contributor")
      .foreach(contributor => cho.addContributor(contributor.agent.name))
    record.producers.foreach(producer => cho.addContributor(producer.name))

    cho.addIdentifier(record.mainIdentifier)

    record.languages
      .collect {
        case lang if lang.sameAs.isDefined =>
          Helper.getLanguageCode(lang.sameAs.get.toString)
      }
      .flatten
      .foreach(code => cho.addLanguage(code))

    record.publishers.foreach(publisher => cho.addPublisher(publisher.name))
    record.regulatedByHolders.foreach(holder => cho.addRight(holder.name))
    record.sources.foreach(cho.addSource)
    record.agentSubjects.foreach(subj => cho.addSubject(subj.name))
    record.conceptSubjects
      .flatMap(concept => concept.prefLabel)
      .foreach(label => cho.addSubject(label._2))

    // Consider only "original" concepts
    record.genres
      .filter(genre => genre.resultsFrom.isEmpty)
      .flatMap(genre => genre.prefLabel)
      .foreach(label => cho.addGenre(label._2))

    record.creationDate
      .collect {
        case d if d.normalizedDateValue.isDefined =>
          s"${d.normalizedDateValue.get}${d.certainty
            .map(c => s"/ $c")
            .getOrElse("")}${d.dateQualifier.map(q => s"/ $q").getOrElse("")}"
        case d if d.expressedDate.isDefined => d.expressedDate.get
      }
      .foreach(cho.addDcTermsCreated)

    record.physicalObject.flatMap(_.duration).foreach(cho.addDcTermsExtent)

    record.issuedDate
      .collect {
        case d if d.normalizedDateValue.isDefined =>
          s"${d.normalizedDateValue.get}${d.certainty
            .map(c => s"/ $c")
            .getOrElse("")}${d.dateQualifier.map(q => s"/ $q").getOrElse("")}"
        case d if d.expressedDate.isDefined => d.expressedDate.get
      }
      .foreach(cho.addDcTermsIssued)

    record.physicalObject
      .foreach(
        _.carrierTypes
          .collect {
            case carrier
                if carrier.resultsFrom.isDefined && carrier.sameAs.isDefined =>
              carrier.sameAs.get.toString
          }
          .foreach(cho.addDcTermsMedium)
      )

    record.spatialCharacteristics.foreach(place =>
      cho.addDcTermsSpatial(place.name)
    )
    record.placesOfCapture.foreach(place => cho.addDcTermsSpatial(place.name))

    record.temporalCharacteristics.collect {
      case d if d.normalizedDateValue.isDefined =>
        s"${d.normalizedDateValue.get}${d.certainty
          .map(c => s"/ $c")
          .getOrElse("")}${d.dateQualifier.map(q => s"/ $q").getOrElse("")}"
      case d if d.expressedDate.isDefined => d.expressedDate.get
    }

    cho.addEdmType(record.ricoType match {
      case FilmType | TVType | VideoType => "VIDEO"
      case FotoType                      => "IMAGE"
      case RadioType | TonType           => "SOUND"
      case TonbildschauType              => "no mapping for audio-visual show"
    })

    record.holders
      .flatMap(Helper.getInstitutionCoord)
      .foreach(coords => cho.addCurrentLocation(coords))

    ElasticSearchClientWrapper
      .getRecordsetName(record.partOf)
      .foreach(cho.addIsPartOf)

    ExtractionResult(cho)
  }
  //scalastyle:on

  private def createWebResources(
      record: Record,
      settings: Settings
  ): ExtractionResult[Option[WebResource]] =
    ExtractionResult(
      record.digitalObject
        .filter(_.distributedOn == VimeoDistributor)
        .map(_.locator)
        .orElse(record.digitalObject.map(_.mainIdentifier))
        .map(WebResource(_, settings))
        .map(webResource => {
          record.digitalObject
            .map(_.usageRegulatedBy)
            .foreach(usage =>
              webResource.addRights(usage.sameAs.map(_.toString))
            )
          record.digitalObject
            .map(_.mimeType)
            .foreach(mimeType => webResource.addDcFormat(mimeType))
          webResource
        })
    )

  def createAggregation(
      record: Record,
      settings: Settings
  ): Option[ExtractionResult[Aggregation]] = {
    val recordIdShort = record.mainIdentifier
    val aggregation = Aggregation(recordIdShort, settings)
    aggregation.addIsShownAt(recordIdShort)
    aggregation.addAggregatedCHO(record.rdfResourceIri)
    record.digitalObject.foreach { obj =>
      {
        aggregation.addIsShownBy(if (obj.distributedOn == VimeoDistributor) {
          obj.locator
        } else {
          obj.mainIdentifier
        })
        record.ricoType match {
          case FotoType =>
            aggregation.addEDMObjectFoto(obj.mainIdentifier)
          case _ =>
            aggregation.addEDMObjectNoFoto(obj.mainIdentifier)
        }
        obj.usageRegulatedBy.sameAs
          .map(_.toString)
          .foreach(aggregation.addRights)
      }
    }

    //fixed value for provider
    aggregation.addProvider("Memoriav")

    record.holders.foreach(id =>
      ElasticSearchClientWrapper
        .getHeldBy(id)
        .map(aggregation.addDataProvider)
    )

    Option(ExtractionResult(aggregation))
  }
}

object EDM {
  def getInstitutionOrRecordsetIdent(ident: String): String =
    ident.substring(ident.lastIndexOf("/") + 1)
}

case class ExtractionResult[T](
    obj: T,
    warnings: mutable.Buffer[String] = mutable.Buffer()
)
