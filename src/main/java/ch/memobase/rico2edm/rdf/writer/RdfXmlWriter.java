package ch.memobase.rico2edm.rdf.writer;

import org.eclipse.rdf4j.common.xml.XMLUtil;
import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.util.Literals;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.rdfxml.RDFXMLWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public class RdfXmlWriter extends RDFXMLWriter {

    public RdfXmlWriter(OutputStream out) {
        super(out);
    }

    @SuppressWarnings("unused")
    public RdfXmlWriter(Writer writer) {
        super(writer);
    }

    String lastWrittenSubjectNamespace = "";
    String getLastWrittenSubjectPrefix = "";

    @Override
    protected void consumeStatement(Statement st) {
        Resource subj = st.getSubject();
        IRI pred = st.getPredicate();
        Value obj = st.getObject();


        // Verify that an XML namespace-qualified name can be created for the
        // predicate
        String predString = pred.toString();
        int predSplitIdx = XMLUtil.findURISplitIndex(predString);
        if (predSplitIdx == -1) {
            throw new RDFHandlerException("Unable to create XML namespace-qualified name for predicate: " + predString);
        }

        String predNamespace = predString.substring(0, predSplitIdx);
        String predLocalName = predString.substring(predSplitIdx);

        try {
            if (!headerWritten) {
                writeHeader();
            }

            // SUBJECT
            if (!subj.equals(lastWrittenSubject)) {
                flushPendingStatements();

                // Write new subject:
                writeNewLine();

                if (st.getPredicate().getLocalName().equalsIgnoreCase("type")) {
                    int indexSeperatorNsPrefix = st.getObject().stringValue().lastIndexOf("/");
                    lastWrittenSubjectNamespace = st.getObject().stringValue().substring(0, indexSeperatorNsPrefix + 1);
                    getLastWrittenSubjectPrefix = st.getObject().stringValue().substring(indexSeperatorNsPrefix + 1);
                    writeStartOfStartTag(lastWrittenSubjectNamespace, getLastWrittenSubjectPrefix);
                } else {
                    writeStartOfStartTag(RDF.NAMESPACE, "Description");
                }


                if (subj instanceof BNode bNode) {
                    writeAttribute(RDF.NAMESPACE, "nodeID", getValidNodeId(bNode));
                } else if (baseIRI != null) {
                    writeAttribute(RDF.NAMESPACE, "about", baseIRI.relativize(subj.stringValue()));
                } else {
                    IRI uri = (IRI) subj;
                    writeAttribute(RDF.NAMESPACE, "about", uri.toString());
                }
                writeEndOfStartTag();
                writeNewLine();

                lastWrittenSubject = subj;
            }

            // PREDICATE
            writeIndent();
            writeStartOfStartTag(predNamespace, predLocalName);

            // OBJECT
            if (obj instanceof Resource objRes) {

                if (objRes instanceof BNode bNode) {
                    writeAttribute(RDF.NAMESPACE, "nodeID", getValidNodeId(bNode));
                } else if (baseIRI != null) {
                    writeAttribute(RDF.NAMESPACE, "resource", baseIRI.relativize(objRes.stringValue()));
                } else {
                    IRI uri = (IRI) objRes;
                    writeAttribute(RDF.NAMESPACE, "resource", uri.toString());
                }

                writeEndOfEmptyTag();
            } else if (obj instanceof Literal objLit) {
                // datatype attribute
                boolean isXMLLiteral = false;

                if (Literals.isLanguageLiteral(objLit)) {
                    //noinspection OptionalGetWithoutIsPresent
                    writeAttribute("xml:lang", objLit.getLanguage().get());
                } else {
                    IRI datatype = objLit.getDatatype();
                    isXMLLiteral = datatype.equals(RDF.XMLLITERAL);

                    if (isXMLLiteral) {
                        writeAttribute(RDF.NAMESPACE, "parseType", "Literal");
                    } else if (!datatype.equals(XSD.STRING)) {
                        writeAttribute(RDF.NAMESPACE, "datatype", datatype.toString());
                    }
                }

                writeEndOfStartTag();

                // label
                if (isXMLLiteral) {
                    // Write XML literal as plain XML
                    writer.write(objLit.getLabel());
                } else {
                    writeCharacterData(objLit.getLabel());
                }

                writeEndTag(predNamespace, predLocalName);
            }

            writeNewLine();

            // Don't write </rdf:Description> yet, maybe the next statement
            // has the same subject.
        } catch (IOException e) {
            throw new RDFHandlerException(e);
        }

    }


    @Override
    protected void flushPendingStatements() throws IOException, RDFHandlerException {
        if (lastWrittenSubject != null) {
            // The last statement still has to be closed:

            //if we have written the last suubject in "EDM style" (direct use of prefix and namespace)
            //we have to close the tag in the same way.
            if (lastWrittenSubjectNamespace.length() > 0 && getLastWrittenSubjectPrefix.length() > 0) {
                writeEndTag(lastWrittenSubjectNamespace, getLastWrittenSubjectPrefix);
                lastWrittenSubjectNamespace = "";
                getLastWrittenSubjectPrefix = "";

            } else {
                writeEndTag(RDF.NAMESPACE, "Description");
            }

            writeNewLine();

            lastWrittenSubject = null;
        }
    }

}
