# Rico to EDM

Transforms data from the internal Rico model to the Europeana Data Model (EDM)

## Background

## Configuration

The application is configured via environment variables. The following variables are mandatory if no default value is
indicated:

* `LOG_LEVEL`: Level of application logging. Valid values
  are `off`, `fatal`, `error`, `warn`, `info`, `debug`, `trace`, `all`. Defaults to `info`.
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap servers (`host1:port1,host2:port2`)
* `TOPIC_IN`: Kafka input topic. Contains document metadata expressed in the internal format
* `TOPIC_OUT`: Kafka output topic where the transformed metadata is written to
* `TOPIC_REPORT`: Kafka reporting topic
* `REPORTING_STEP_NAME`: Name of service as used in reports
* `ELASTIC_HOST`: Elasticsearch host list
* `ELASTIC_CLUSTER_NAME`: Elasticsearch cluster name
* `ELASTIC_API_KEY_ID`: ID part of the API key used to authenticate with Elasticsearch
* `ELASTIC_API_KEY_SECRET`: Secret part of the API key used to authenticate with Elasticsearch
* `ELASTIC_CA_CERT_PATH`: Path to the CA certificate used to verify Elasticsearch's SSL certificate
* `RECORD_SET_INDEX`: Elastic index of the Memobase record set metadata
* `INSTITUTION_INDEX`: Elastic index of the Memobase institution metadata
* `EXPORT_DEFINITION_RULES_PATH`: Path to the JSON file containing the export definition rules
* `INSTITUTION_COORDINATES_PATH`: Path to the CSV-based mapping
* `ISOCODE_MAPPING_PATH`: Path to the the CSV-based language mapping
* `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `SSL_KEYSTORE_KEY`: Private key in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `SSL_KEY_PASSWORD`
* `SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `SSL_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `SSL_KEYSTORE_TYPE` and at the path defined in `SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `SSL_TRUSTSTORE_TYPE` and at the path defined in `SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `SSL_TRUSTSTORE_CERTIFICATES`

### Export rules

### Institution links

### Isocode mapping

### Mapping Rules

[rules](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/270368788/Mapping+Memobase+to+EDM)

### Implementation

* Scala
* Kafka Streams
